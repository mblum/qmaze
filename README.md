Q-learning Demo

This MATLAB code demonstrates Q-learning, an algorithm for model-free reinforcement learning. It solves the problem of finding the shortest path to a specified goal state. The maze and the goal state(s) can be specified in a text file. To run the demo open MATLAB and type 'main'.