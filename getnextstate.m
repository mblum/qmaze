function next_state = getnextstate(state, action, maze)

[i,j] = ind2sub(size(maze), state);

if action == 1
    i = i - 1;
elseif action == 2
    j = j - 1;
elseif action == 3
    i = i + 1;
elseif action == 4
    j = j + 1;
end

next_state = sub2ind(size(maze), i, j);

if (maze(next_state) == 2)
    next_state = state;
end

