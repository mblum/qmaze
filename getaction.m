function action = getaction(state, Q, epsilon)

% get maximum Q value
m = max(Q(state, :));
% get indices of maxima
arg_m = Q(state, :) == m;
% initialize actions
actions = 1:length(arg_m);

if (rand >= epsilon)
    % choose best actions
    actions = actions(arg_m);
end

% choose random action from actions
action = actions(randi(length(actions)));