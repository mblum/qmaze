function maze_with_agent = initialposition(maze)

while (1)
    
    % choose random field
    x = randi(size(maze, 1));
    y = randi(size(maze, 2));
    
    % if field is empty exit loop
    if (maze(x, y) == 0)
        break;
    end
end

% copy maze
maze_with_agent = maze;

% put agent on filed x,y
maze_with_agent(x, y) = 1;

