function maze = loadmaze(filename)

% open file
fid = fopen(filename, 'r');

maze_def = [];

while (1)
    % read line
    line = fgetl(fid);
    if line == -1
        break;
    end
    maze_def = [maze_def; line];
end
fclose(fid);

% walls (*) are encoded by 2
% goals (g) are encoded by 3
% free fields are 0
maze = (maze_def == '*') * 2 + (maze_def == 'g') * 3;
