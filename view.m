function view (maze, Q)

% plot agent in maze
subplot(1, 2, 1)
colormap('hot')
imagesc(maze)
axis image
title('Maze')

% plot Q-table
subplot(1, 2, 2)
image(reshape(max(Q, [], 2), size(maze)).^2, 'CDataMapping','scaled')
axis image
caxis([0.2 1.0])
title('Q-table')

drawnow

pause(0.1)
