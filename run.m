function Q = run(maze, viewer, Q, p, epsilon)

% set agent on a random position
maze_with_agent = initialposition(maze);

% show visualization if needed
if (viewer), view(maze_with_agent, Q), end

for j=1:p.num_steps
    
    state = find(maze_with_agent == 1);
    action = getaction(state, Q, epsilon);
    next_state = getnextstate(state, action, maze);
    reward = (maze_with_agent(next_state) == 3);
    
    maze_with_agent(state) = 0;
    maze_with_agent(next_state) = 1;
    
    % show visualization if needed
    if (viewer), view(maze_with_agent, Q), end
    
    % check for final state and apply learning rule
    if (reward ~= 0)
        Q(state, action) = (1 - p.alpha) * Q(state, action) + p.alpha * reward;
        break;
    end
    Q(state, action) = (1 - p.alpha) * Q(state, action) + p.alpha * (reward + p.gamma * max(Q(next_state, :)));
    
end
