% parameter setting
filename       = 'maze';
p.alpha        = 0.2;
p.gamma        = 0.95;
p.epsilon      = 0.2;
p.num_steps    = 50;
num_episodes   = 2000;
test_frequency = 50;

% load maze from file
maze = loadmaze(filename);

% initialize Q function to one
Q = ones(length(maze(:)), 4);
% set Q table entry to zero for wall
% (for better visualization)
Q(maze(:) == 2, :) = 0;

for i=1:num_episodes
    
    % do testing
    if mod(i, test_frequency) == 1
        run(maze, 1, Q, p, 0);
    end
    
    % do training
    Q = run(maze, 0, Q, p, p.epsilon);
    
    % show visualization
    view(maze, Q)
    
end